## SQL ##

As much as possible, you should be live coding. You can definitely use powerpoint, but try to come up with additional examples or expand on what it is in the slides by demonstrating live in PG Admin.

Also, make sure students can do the questions with you. Dont just run queries for them if avoidable. Try to make sure they are either typing your examples out themselves if theyre short or youre sending them queries to run if theyre longer.

Be sure to thoroughly describe what is happening with the queries, regularly referencing the structure diagram: https://www.postgresql.org/docs/9.6/sql-select.html

## Intro to SQL ##


Prerequisite: install postgresql and PG Admin

Suggestions: display pgadmin and slides on your screen in a split screen manner

 - print out the queries you will run for each topic and type them in (dont copy and paste them during the session)

#### 0. DDL & DML (5 minutes) ####
Concepts that likely won't come up often in day to day work, but good to understand this is what you are using when using SQL

When you run statements that have to do with the structure of tables or the database itself, be that creating or altering tables, setting constraints, or dropping tables entirely, that's DDL

When you run statements that have to do with the data itself within tables such as inserting, updating, deleting, or selecting, that's DML

#### 1. Creating tables, basic constraints, and inserting data (10 minutes) ####

- Show students how to load sample database and tables (dvdrental)
- Talk about the structure of creating and inserting (slides)
- Create a sample table and insert some sample data with the following queries:
```sql
CREATE TABLE cats (
cat_id integer NOT NULL UNIQUE,
name VARCHAR NOT NULL,
title VARCHAR DEFAULT 'mouser', 
birth_date DATE,
PRIMARY KEY (cat_id));
```
	
```sql
INSERT INTO cats (cat_id, name, title, birth_date)
VALUES (1,'Chairman Meow','chairman', '2015-01-01'),
	   (2,'Morris Katz','physicist','2017-03-04'),
	   (3,'Larry',DEFAULT,'2007-01-15');
```

#### 1.5 Deleting data  ####
- Explain the basic structure from the slides then go over an example from the table we created in step 1

```sql
CREATE TABLE cats (
cat_id integer NOT NULL UNIQUE,
name VARCHAR NOT NULL,
title VARCHAR DEFAULT 'mouser', 
birth_date DATE,
PRIMARY KEY (cat_id));

INSERT INTO cats (cat_id, name, title, birth_date)
VALUES (1,'Chairman Meow','chairman', '2015-01-01'),
	   (2,'Morris Katz','physicist','2017-03-04'),
	   (3,'Larry',DEFAULT,'2007-01-15');

Select cat_id, name, title, birth_date from  cats;

Delete from cats;

Delete from cats where cat_id = 2;

Drop Table cats;

```

#### 2. Select statements, aliasing, where clause, filtering statements (AND,OR,LIKE), limit, between, order by (30 minutes) ####
- Explain the basic structure from the slides then go over an example from the schema we ingested
Select and limiting
```sql
SELECT *
FROM film
LIMIT 100
```

aliasing
```sql 
select film_id, title, description, release_year, (replacement_cost - rental_rate) as max_loss
from film
limit 100
```
What happens if we dont alias that calculated column?

Why do you think we alias tables?
```sql 
select film_id, title, description, release_year, (replacement_cost - rental_rate) as max_loss
from film as f
limit 100
```

Filtering
```sql 
select film_id, title, description, release_year, (replacement_cost - rental_rate) as max_loss
from film as f
where (replacement_cost - rental_rate) >20
```
can't filter on max_loss, must use calculation again. Why?

How would we filter the original table to just get those films released in or after 2006?
```sql
select * 
from film
where release_year >= 2006
```
What other realease years are in this dataset? How do we find out?
```sql
select distinct release_year 
from film
```

Between and In
```sql
select film_id, title, description, release_year, replacement_cost, rental_rate, (replacement_cost - rental_rate) as max_loss
from film
where (replacement_cost - rental_rate) between 10 and 20
```
Between is inclusive of it's bounds

```sql
select film_id, title, description, rating
from film
where rating IN ('G', 'PG', 'PG-13')
```

Like and Wildcards

% for multiple character wildcards
_ for single character wildcards
```sql
select film_id, title, description, release_year
from film
where title LIKE 'a%'
```
case sensitive

Order By

mention that the default is order by ascending
```sql
select film_id as id
		, title
		, description
		, release_year as released
		, replacement_cost
		, rental_rate
		, (replacement_cost - rental_rate) as max_loss
from film
order by replacement_cost - rental_rate
limit 100
```

#### 3. Practice (15 minutes) ####
		Questions are in slides. Give students 10 minutes to do the questions (2 or 3), and have the students share their answers in the remaining 5 minutes.
		You can have someone share his or her screen and explain one of the answers, then move to another person to share the next.
1.
```sql
select * 
from customer
where last_name LIKE 'T%'
order by first_name asc
```
2.
```sql
select * 
from rental
where return_date between '05-28-2005' and '06-01-2005'

```
3.
```sql
select film_id, store_id
from inventory
order by film_id


select count(*), f.title, f.film_id
from film f
join inventory i
on i.film_id = f.film_id
group by f.title, f.film_id
order by count(*) desc
```
can't do it with only what we've talked about so far


#### 4. Set-based thinking (5 minutes) ####
Explain that set-based thinking is different from traditional programming thinking (procedural thinking). There are still things in SQL that allow for us to
use procedural thinking (cursors), but we use them almost never. Cursors are only used in modern times in very few scenarios when memory needs to be kept particularly low.

Think of tables as sets and queries as pulling subsets from those sets, with queries also setting the conditions for those subsets

For each row in Table A, lookup the corresponding row in Table B
vs. 
Join Table A and Table B

Can perform aggregate calculations on our sets of data i.e. sum, average, min, max, etc, perform operations on entire sets at a time, rather than going through 1 at a time

PLSQL or "Procedural Language" SQL does exist and functions as you would expect a standard programming language to with things like for loops and if statements, but operates within the bounds of a database. 
Used for ETL packages to automate movement and modification of data

#### 5. Joins, Unions, and Aggregates (group by, having, avg, count, min, max, etc)(30 minutes) ####
JOINS
Joins match records from two (or more) tables by putting the records together where a condition is met, most often a "key" value is the same in each table.
By using joins, you can get information from table B that is related to a record in table A; important for the concept of normalization, reducing the amount of redundant data
A record in one table can be joined to multiple records in another table, or none at all
Can specify fields to return from any table involved in a join

JOIN is equivalent to INNER JOIN and is the most common type of join; Only get results where the join condition is met
LEFT/RIGHT JOIN: Get results where join is successful, but also get results that have no join from one table (either left (1st) or right (2nd)), and all the fields from the other table are NULL for these records
[FULL] OUTER JOIN: Get results of successful joins, any records from either table that do not have a join are still included in the result set, with the fields from the other table all NULL

```sql
--Small example joins
drop table if exists products;
create table products(
	product_id serial,
	product_name varchar(50),
	price decimal,
	primary key(product_id)
);

drop table if exists orders;
create table orders(
	order_id serial,
	product_id integer,
	amount integer,
	primary key(order_id)
	--foreign key(product_id) references products(product_id)
);


insert into products(product_name, price) values
	('Burger', '5.00'),
	('Drink', '1.00'),
	('Chicken Tendies', '6.00'),
	('Fries', '1.50')
;

select * from products

insert into orders(product_id, amount) values
	(1, 4), 
	(2, 2),
	(3, 7),
	(1, 3),
	(1, 2),
	(3, 4),
	(5, 1)
;

select * from orders
select * from products

select p.*, o.* 
from products p
join orders o
on p.product_id = o.product_id

select *
from products p
left join orders o
on p.product_id = o.product_id


select *
from products p
full outer join orders o
on p.product_id = o.product_id
```

Joins are the core of relational database systems


UNIONS
If joins add data horizontally, unions add data vertically
In simple terms, a union is stacking on set of data on top of a similar set of data

In order to successfully union, the two sets of data that you are attempting to combine must have exactly the same fields
you can alias similar fields to the same name in order for this to work i.e. alias "FullName" to "Name" to union with a set that has a "Name" field

UNION by itself eliminates duplicate rows
can use UNION ALL to keep duplicate rows

```sql
SELECT film_id, title, description, rating
from film
where rating IN ('G', 'PG')
UNION (ALL)
select film_id, title, description, rating
from film
where rating in ('PG', 'PG-13')
```
Not a great example as you could get the same results from one query, but illustrates the idea

INTERSECT
Read off slide mostly
While joins are between two sets with different fields and generally one or a few in common, an intersect could be seen as a join, but every single field of each table is a part of the join condition
same results as if you were to UNION ALL but only took the duplicate results

```sql
SELECT film_id, title, description, rating
from film
where rating IN ('G', 'PG')
intersect
select film_id, title, description, rating
from film
where rating in ('PG', 'PG-13')
```

```sql
select distinct i.film_id
from film f
inner join inventory i
on f.film_id = i.inventory_id

select f.film_id
from film
intersect
select film_id
from inventory
```
difficult to illustrate with the join, because our database does not have tables with more than 1 field in common

AGGREGATES & GROUP BY
GROUP BY does pretty much exactly what it sounds like, you can condense your result set and perform aggregate calculations by grouping your results by specified fields
When using aggregates, every column must be used in an aggregate function, or as part of the GROUP BY clause

Thinking of each group as a new subset, the HAVING clause applies filters to each group. i.e. having count(*) > 5 to get all groups that have more than 5 results
can use HAVING on aggregates as well

```sql
select c.first_name, c.last_name, count(*)
from customer c
join payment p
on c.customer_id = p.customer_id
group by c.first_name, c.last_name
having count(*) > 30
order by count(*) desc
```

```sql
select 	c.first_name
		, c.last_name
		, count(*)
		, Round(avg(p.amount)::numeric, 2)
from customer c
join payment p
on c.customer_id = p.customer_id
group by c.first_name, c.last_name
having avg(amount) < 4 
order by avg(amount) desc
```

#### 6. Practice (20 minutes) ####
Questions in slides.

1
```sql
select c.customer_id, c.first_name, c.last_name, sum(p.amount) as Total
from customer c
join payment p
on c.customer_id = p.customer_id
group by c.customer_id, c.first_name, c.last_name
order by sum(p.amount) desc
```
2
```sql
select 	concat(a.first_name, ' ', a.last_name) as actor_name
		, count(*) as movies_in
from actor a
join film_actor fa
on a.actor_id = fa.actor_id
join film f
on f.film_id = fa.film_id
where f.release_year = 2006
group by actor_name
order by count(*) desc
```

#### 7. Subqueries(10 minutes) ####
subqueries essentially become new temporary tables
can use subqueries where you would use tables
can use subqueries with one result (generally an aggregate) in where clause in place of a value 
can use subqueries with only one field as a set for IN condition

want to find films where the replacement cost is higher than average
```sql
SELECT film_id, title, replacement_cost
FROM film
WHERE replacement_cost > (SELECT AVG(replacement_cost) FROM film);
```

```sql
select count(*)
from (select * 
	  from film f
	  join film_actor fa
	  on f.film_id = fa.film_id
	  join actor a
	  on fa.actor_id = a.actor_id)z
```

#### 8. Practice (20 minutes) ####
Questions in slides.
1
```sql
select f.film_id, f.title, f.length
from film f
join inventory i
on f.film_id = i.film_id
join rental r
on r.inventory_id = i.inventory_id
where r.return_date between '05-15-2005' and '05-31-2005'
```
2
```sql
select avg(f.rental_rate), count(*), a.first_name, a.last_name
from actor a
join film_actor fa
on a.actor_id = fa.actor_id
join film f
on fa.film_id = f.film_id
group by a.first_name, a.last_name
having avg(f.rental_rate) > 3 AND count(*) > 20
```


#### 9. Break (10 minutes) ####

#### 10. Explain Plans (5 minutes) ####
Talk about how they work and show one using a previous query. 

Can run EXPLAIN to see step by step plan of how your statement will execute. Running EXPLAIN ANALYZE will not only show the step by step plan, but also execute the statement and compare the actual execution to the planner.

PGAdmin also has an option for a graphical explain plan


#### 11. Views (5 minutes) ####
Views are tables that are compiled at runtime. 

#### 12. check, Primary key and foreign key constraints (15 minutes) ####
Check Constraints
All of the same operators and functions that can be used in where clause can be used in check constrainst >, <, =, <>, IN, BETWEEN, LIKE, etc.
```sql
create table cats (
	cat_id Integer NOT NULL UNIQUE, --serial
	name VARCHAR not null,
	title VARCHAR DEFAULT 'mouser',
	birth_date DATE,
	PRIMARY KEY(cat_id),
	check (birth_date < '01-01-2022')
);
```
change birth date constraint to 2016 to illustrate error when inputting data

ALTER
```sql
alter table cats
add check (birth_date < '01-01-2016')
--or
alter table cats
add constraint birth_date_check CHECK (birth_date < '01-01-2022')
```

PRIMARY KEY and FOREIGN KEY
 
identify primary keys in dvdrental ERD
what tables have composite primary keys? aka what tables have primary keys consisting of more than 1 field

- Talk about the possible ways to add primary key constraints to our cats table: 

```sql
	CREATE TABLE cats (
    cat_id Integer NOT NULL UNIQUE,
    name VARCHAR NOT NULL,
	title VARCHAR DEFAULT 'mouser', 
	birth_date DATE,
    PRIMARY KEY (cat_id));
```
	
	vs.
	
```sql 
	ALTER TABLE cats
	ADD PRIMARY KEY (cat_id); 
```
	
- show primary and foreign key contraints in the data model(attached)
- Bring up candidate key if time
- Show how to view and drop constraints also
view constraints
```sql
SELECT con.*
       FROM pg_catalog.pg_constraint con
            INNER JOIN pg_catalog.pg_class rel
                       ON rel.oid = con.conrelid
            INNER JOIN pg_catalog.pg_namespace nsp
                       ON nsp.oid = connamespace
       WHERE --nsp.nspname = '<schema name>'
             /*AND*/ rel.relname = 'cats';
```
drop constraints
```sql
alter table cats
drop constraint birth_date_check
```

#### 13. Practice (15 minutes) ####

#### 14. Indexes (10 minutes) ####
Talk about how the B-tree index works
https://dzone.com/articles/database-btree-indexing-in-sqlite

#### 15. Practice (20 minutes) ####
1
```sql
select 	s.first_name
		, s.last_name
		, s.staff_id
		, count(r.rental_id) rental_count
		, count(p.payment_id) payment_count
		, count(r.rental_id) - count(p.payment_id) outstanding
from staff s
join rental r
on s.staff_id = r.staff_id
left join payment p
on r.rental_id = p.rental_id
group by s.first_name, s.last_name, s.staff_id
```
2
```sql
select c.name, sum(f.rental_rate), count(*) total_rentals
from category c
join film_category fc
on c.category_id = fc.category_id
join film f
on fc.film_id = f.film_id
join inventory i
on f.film_id = i.film_id
join rental r
on r.inventory_id = i.inventory_id
group by c.name
order by total_rentals desc
```

#### 16. Secure Sockets Layer (SSL) ####
Read from slide

select ssl_is_used(); will tell you if ssl is being used on a particular db


#### 17. Transactions
Review creating transactions using either the cats example above, or follow along with https://www.postgresqltutorial.com/postgresql-transaction/

#### 18. Functions
Review the basic structure of a function
Discuss what a Dollar quoted string constant is and what it can be used for

```sql

create function find_film_by_id(
   id int
) returns film 
language sql
as 
  'select * from film 
   where film_id = id';

--If the function has many statements, it can be hard to read since the query has quotes in it
--Dollar quoted string constants allow you to avoid  escaping every single quote and backslash

create [or replace] function  function_name(param_list)
    returns return_type
    language plpgsql  
As
$$
declare 
-- variable declaration
begin 
-- logic
end;
$$
```
```sql
CREATE or Replace FUNCTION make_full_name(first_name text, last_name text, middle_name text DEFAULT '')
RETURNS text
AS
$$
 SELECT (first_name || ' ' || middle_name || ' ' || last_name);   
$$
LANGUAGE SQL;

--DROP FUNCTION make_full_name;

select public.make_full_name('George', 'Carver', 'Washington');
```



#### EXTRA TIME (15 minutes) ####
In case anything needs to be covered a bit more in depth, this session was planned to have 15 extra minutes of buffer.